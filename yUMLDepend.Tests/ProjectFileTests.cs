using System;
using NUnit.Framework;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;

namespace yUMLDepend.Tests
{
	public class ProjectFileTests
	{
		[TestFixture]
		public class WhenConstructedWithFileWhichIsNotAnXmlFile
		{
			private IProjectFile _projectFile;
			
			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/NotAnXmlFile.xml");
				
				_projectFile = new ProjectFile(testFile);
			}
			
			[Test]
			public void ThenANotSupportedExceptionIsThrown() {
				Assert.Throws<NotSupportedException>(() => _projectFile.GetProjectDependencies().Enumerate());
			}
		}

		[TestFixture]
		public class WhenConstructedWithAnEmptyXmlFile
		{
			private IProjectFile _projectFile;
			private IEnumerable<Dependency> _dependencies;
			
			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/EmptyXmlFile.xml");

				_projectFile = new ProjectFile(testFile);
				_dependencies = _projectFile.GetProjectDependencies();
			}
			
			[Test]
			public void ThenANotSupportedExceptionIsThrown() {
				Assert.That(_dependencies.IsEmpty());
			}
		}

		[TestFixture]
		public class WhenConstructedWithFileWithNoProjectReferences_AndHasRootNamespace 
		{
			private IProjectFile _projectFile;
			private IEnumerable<Dependency> _dependencies;
			
			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/NoProjectReferences.xml");
				
				_projectFile = new ProjectFile(testFile);
				_dependencies = _projectFile.GetProjectDependencies();
			}
			
			[Test]
			public void ThenThereAreNoDependencies() {
				Assert.That(_dependencies.IsEmpty());
			}
		}

		[TestFixture]
		public class WhenConstructedWithFileWithProjectReferences_AndNoRootNamespace 
		{
			private IProjectFile _projectFile;
			private IEnumerable<Dependency> _dependencies;
			
			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/NoRootNamespace.xml");
				
				_projectFile = new ProjectFile(testFile);
				_dependencies = _projectFile.GetProjectDependencies();
			}
			
			[Test]
			public void ThenThereAreNoDependencies() {
				
				Assert.That(_dependencies.IsEmpty());
			}
		}

		[TestFixture]
		public class WhenConstructedWithFileWithNoProjectReferences_AndNoRootNamespace 
		{
			private IProjectFile _projectFile;
			private IEnumerable<Dependency> _dependencies;
			
			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/NoRootNamespaceOrProjectReferences.xml");
				
				_projectFile = new ProjectFile(testFile);
				_dependencies = _projectFile.GetProjectDependencies();
			}
			
			[Test]
			public void ThenThereAreNoDependencies() {
				
				Assert.That(_dependencies.IsEmpty());
			}
		}

		[TestFixture]
		public class WhenConstructedWithFileWithProjectReferences_AndHasRootNamespace
		{
			private IProjectFile _projectFile;
			private IEnumerable<Dependency> _dependencies;

			[SetUp]
			public void SetUp() {
				var testFile = 
					Path.Combine(TestContext.CurrentContext.TestDirectory + "/TestFiles/HasRootNamespaceAndProjectReferences.xml");

				_projectFile = new ProjectFile(testFile);
				_dependencies = _projectFile.GetProjectDependencies();
			}

			[Test]
			public void ThenThereAreDependencies() {
				Assert.That(_dependencies.IsNotEmpty());
			}

			[Test]
			public void ThenThereAreTheCorrectNumberOfDependenciesCreated() {
				Assert.That(_dependencies.Count() == 3);
			}

			[Test]
			public void ThenAllTheDependenciesHaveTheCorrectProject() {
				Assert.That(_dependencies.All(d => d.Project == "TestProjectA"));
			}
		}
	}
}

