using System;
using NUnit.Framework;
using Moq;
using System.IO;
using System.Linq;

namespace yUMLDepend.Tests
{
	public class yUMLWebDiagramGeneratorTests
	{
		[TestFixture]
		[IntegrationTestFixture]
		public class WhenGeneratingDiagram_WithEmptySetOfDependencies {
			private IyUMLDiagramGenerator _yumlDiagramGenerator;
			private Mock<IyUMLDependencyFormatter> _mockDependencyFormatter;
			private Stream _diagram;

			[SetUp]
			public void SetUp() {
				_mockDependencyFormatter = new Mock<IyUMLDependencyFormatter>();

				_yumlDiagramGenerator = new yUMLWebDiagramGenerator(_mockDependencyFormatter.Object);
				_diagram = _yumlDiagramGenerator.GenerateDiagram(Enumerable.Empty<Dependency>());
			}

			[Test]
			public void ThenADiagramIsReturned() {
				Assert.NotNull(_diagram);
			}
		}

		[TestFixture]
		public class WhenGeneratingDiagram_WithNullDependencies {
			private IyUMLDiagramGenerator _yumlDiagramGenerator;
			private Mock<IyUMLDependencyFormatter> _mockDependencyFormatter;
			
			[SetUp]
			public void SetUp() {
				_mockDependencyFormatter = new Mock<IyUMLDependencyFormatter>();
				
				_yumlDiagramGenerator = new yUMLWebDiagramGenerator(_mockDependencyFormatter.Object);
			}
			
			[Test]
			public void ThenArgumentNullExceptionIsThrown() {
				Assert.Throws<ArgumentNullException>(() => _yumlDiagramGenerator.GenerateDiagram(null));
			}
		}
	}
}

