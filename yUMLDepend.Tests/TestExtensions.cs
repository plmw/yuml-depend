using System;
using System.Collections.Generic;
using System.Linq;

namespace yUMLDepend.Tests
{
	internal static class TestExtensions
	{
		internal static bool IsEmpty<T>(this IEnumerable<T> enumerable) {
			return enumerable.Count() == 0;
		}

		internal static bool IsNotEmpty<T>(this IEnumerable<T> enumerable) {
			return enumerable.Count() > 0;
		}
	}
}

