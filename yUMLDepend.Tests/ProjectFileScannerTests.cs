using System;
using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using System.Linq;
using yUMLDepend.FileSystem;

namespace yUMLDepend.Tests
{
	public class ProjectFileScannerTests {

		[TestFixture]
		[IntegrationTestFixture]
		public class WhenGettingProjectFilesForPath_AndThereAreNoMatchingFilesInPath
		{
			private Mock<IDirectory> _mockDirectory;
			private IProjectFileScanner _projectFileScannerUnderTest;
			private IEnumerable<string> _testResults;

			private const string TestPath = "test path";

			[SetUp]
			public void SetUp() {
				_mockDirectory = new Mock<IDirectory>();

				_mockDirectory.Setup(m => m.GetFiles(It.IsAny<string>()))
									 .Returns(Enumerable.Empty<string>());

				_projectFileScannerUnderTest = new ProjectFileScanner(s => _mockDirectory.Object);

				_testResults = _projectFileScannerUnderTest.GetProjectFiles(TestPath);
			}
			
			[Test]
			public void ThenItGetsFilesFromThePath() {
				_mockDirectory.Verify(m => m.GetFiles(It.IsAny<string>()), Times.Once());
			}

			[Test]
			public void ThenItReturnsEmptyListOfProjectFilePaths() {
				Assert.That(_testResults.IsEmpty());
			}
		}

		[TestFixture]
		[IntegrationTestFixture]
		public class WhenGettingProjectFilesForPath_AndThereAreProjectFiles_AndNothingIsExcluded
		{
			private Mock<IDirectory> _mockDirectory;
			private IProjectFileScanner _projectFileScannerUnderTest;
			private IEnumerable<string> _testResults;
			
			private const string TestProjectFile = "test project file";
			private const string TestPath = "test path";
			
			[SetUp]
			public void SetUp() {
				_mockDirectory = new Mock<IDirectory>();

				_mockDirectory.Setup(m => m.GetFiles(It.IsAny<string>()))
					.Returns(new List<string> { TestProjectFile });
				
				_projectFileScannerUnderTest = new ProjectFileScanner(s => _mockDirectory.Object);
				
				_testResults = _projectFileScannerUnderTest.GetProjectFiles(TestPath, null);
			}
			
			[Test]
			public void ThenItGetsFilesFromThePath() {
				_mockDirectory.Verify(m => m.GetFiles(It.IsAny<string>()), Times.Once());
			}

			[Test]
			public void ThenItOReturnsEmptyListOfProjectFilePaths() {
				Assert.That(_testResults.IsNotEmpty());
			}
		}

		[TestFixture]
		[IntegrationTestFixture]
		public class WhenGettingProjectFilesForPath_AndThereAreProjectFiles_AndTheyAreNotExcluded
		{
			private Mock<IDirectory> _mockDirectory;
			private IProjectFileScanner _projectFileScannerUnderTest;
			private IEnumerable<string> _testResults;
			
			private const string TestProjectFile = "test project file";
			private const string TestPath = "test path";
			
			[SetUp]
			public void SetUp() {
				_mockDirectory = new Mock<IDirectory>();

				_mockDirectory.Setup(m => m.GetFiles(It.IsAny<string>()))
					.Returns(new List<string> { TestProjectFile });
				
				_projectFileScannerUnderTest = new ProjectFileScanner(s => _mockDirectory.Object);
				
				_testResults = _projectFileScannerUnderTest.GetProjectFiles(TestPath, "expression which won't match");
			}
			
			[Test]
			public void ThenItGetsFilesFromThePath() {
				_mockDirectory.Verify(m => m.GetFiles(It.IsAny<string>()), Times.Once());
			}

			[Test]
			public void ThenItOReturnsEmptyListOfProjectFilePaths() {
				Assert.That(_testResults.IsNotEmpty());
			}
		}

		[TestFixture]
		[IntegrationTestFixture]
		public class WhenGettingProjectFilesForPath_AndThereAreProjectFiles_ButTheyAreExcluded
		{
			private Mock<IDirectory> _mockDirectory;
			private IProjectFileScanner _projectFileScannerUnderTest;
			private IEnumerable<string> _testResults;

			private const string TestProjectFile = "test project file";
			private const string TestPath = "test path";
			
			[SetUp]
			public void SetUp() {
				_mockDirectory = new Mock<IDirectory>();

				_mockDirectory.Setup(m => m.GetFiles(It.IsAny<string>()))
					.Returns(new List<string> { TestProjectFile });
				
				_projectFileScannerUnderTest = new ProjectFileScanner(s => _mockDirectory.Object);
				
				_testResults = _projectFileScannerUnderTest.GetProjectFiles(TestPath, ".*");
			}
			
			[Test]
			public void ThenItGetsFilesFromThePath() {
				_mockDirectory.Verify(m => m.GetFiles(It.IsAny<string>()), Times.Once());
			}
			
			[Test]
			public void ThenItOReturnsEmptyListOfProjectFilePaths() {
				Assert.That(_testResults.IsEmpty());
			}
		}
	}
}

