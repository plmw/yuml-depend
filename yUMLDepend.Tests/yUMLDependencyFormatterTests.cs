using System;
using NUnit.Framework;

namespace yUMLDepend.Tests
{
	public class yUMLDependencyFormatterTests
	{
		[TestFixture]
		public class WhenFormattingADependency {
			private IyUMLDependencyFormatter _yumlDependencyFormatter;
			private const string testProject = "test project";
			private const string testDependentOn = "test dependent on";
			private Dependency _testDependency;
			private string _result;
			
			[SetUp]
			public void SetUp() {
				_yumlDependencyFormatter = new yUMLDependencyFormatter();
				_testDependency = new Dependency(testProject, testDependentOn);
				_result = _yumlDependencyFormatter.Format(_testDependency);
			}
			
			[Test]
			public void ThenAResultIsProduced() {
				Assert.NotNull(_result);
			}

			[Test]
			public void ThenACorrectlyFormattedResultIsProduced() {
				Assert.AreEqual("[test project]->[test dependent on]", _result);
			}
		}

		[TestFixture]
		public class WhenFormattingANullDependency {
			private IyUMLDependencyFormatter _yumlDependencyFormatter;
			
			[SetUp]
			public void SetUp() {
				_yumlDependencyFormatter = new yUMLDependencyFormatter();
			}
			
			[Test]
			public void ThenAnArgumentNullExceptionIsThrown() {
				Assert.Throws<ArgumentNullException>(() => _yumlDependencyFormatter.Format(null));
			}
		}
	}
}

