using System;
using NUnit.Framework;

namespace yUMLDepend.Tests
{
	[TestFixture]
	public class DependencyTests
	{
		[Test]
		public void DependencyMustHaveNonNullProjectAndDependentOn()
		{
			Assert.Throws<ArgumentNullException>(() => new Dependency(null, ""));
			Assert.Throws<ArgumentNullException>(() => new Dependency("", null));
			Assert.Throws<ArgumentNullException>(() => new Dependency(null, null));
		}
	}
}

