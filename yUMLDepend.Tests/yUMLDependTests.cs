using System;
using NUnit.Framework;
using System.IO;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace yUMLDepend.Tests
{
	public class yUMLDependTests
	{
		[TestFixture]
		public class WhenRunningyUMLProcess_OnPathWithNoProjectFiles
		{
			private yUMLDepend _yUMLDepend;
			private Mock<IProjectFile> _mockProjectFile;
			private Mock<IProjectFileScanner> _mockProjectFileScanner;
			private Mock<IyUMLDiagramGenerator> _mockDiagramGenerator;
			private const string _testBasePath = "test base path";
			private const string _testSavePath = "test save path";

			[SetUp]
			public void SetUp()
			{
				_mockProjectFile = new Mock<IProjectFile>();
				_mockProjectFileScanner = new Mock<IProjectFileScanner>();
				_mockDiagramGenerator = new Mock<IyUMLDiagramGenerator>();

				_mockDiagramGenerator.Setup(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()))
				                     .Returns(new MemoryStream());

				_yUMLDepend = new yUMLDepend(f => new MemoryStream(),
				                             f => _mockProjectFile.Object,
				                             _mockProjectFileScanner.Object,
				                             _mockDiagramGenerator.Object);

				_yUMLDepend.Process(_testBasePath, _testSavePath);
			}
			
			[Test]
			public void ThenTheProjectFileScannerIsUsedToScanBasePath()
			{
				_mockProjectFileScanner.Verify(m => m.GetProjectFiles(_testBasePath, null), Times.Once());
			}

			[Test]
			public void ThenProjectFilesAreNotInterrogatedForDependencies()
			{
				_mockProjectFile.Verify(m => m.GetProjectDependencies(), Times.Never());
			}

			[Test]
			public void ThenTheDiagramGeneratorIsNeverUsed()
			{
				_mockDiagramGenerator.Verify(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()), Times.Never());
			}
		}

		[TestFixture]
		public class WhenRunningyUMLProcess_OnPathWithSomeProjectFiles_ButWithNoDependencies
		{
			private yUMLDepend _yUMLDepend;
			private Mock<IProjectFile> _mockProjectFile;
			private Mock<IProjectFileScanner> _mockProjectFileScanner;
			private Mock<IyUMLDiagramGenerator> _mockDiagramGenerator;
			private const string _testBasePath = "test base path";
			private const string _testSavePath = "test save path";
			private List<string> _testFiles = new List<string> { "test project file1", "test project file2" };
			
			[SetUp]
			public void SetUp()
			{
				_mockProjectFile = new Mock<IProjectFile>();
				_mockProjectFileScanner = new Mock<IProjectFileScanner>();
				_mockDiagramGenerator = new Mock<IyUMLDiagramGenerator>();

				_mockProjectFileScanner.Setup(m => m.GetProjectFiles(_testBasePath, null))
				                       .Returns(_testFiles);

				_mockDiagramGenerator.Setup(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()))
					.Returns(new MemoryStream());
				
				_yUMLDepend = new yUMLDepend(f => new MemoryStream(),
				                             f => _mockProjectFile.Object,
				                             _mockProjectFileScanner.Object,
				                             _mockDiagramGenerator.Object);
				
				_yUMLDepend.Process(_testBasePath, _testSavePath);
			}
			
			[Test]
			public void ThenTheProjectFileScannerIsUsedToScanBasePath()
			{
				_mockProjectFileScanner.Verify(m => m.GetProjectFiles(_testBasePath, null), Times.Once());
			}
			
			[Test]
			public void ThenEachProjectFileIsInterrogatedForDependencies()
			{
				_mockProjectFile.Verify(m => m.GetProjectDependencies(), Times.Exactly(2));
			}

			[Test]
			public void ThenTheDiagramGeneratorIsNeverUsed()
			{
				_mockDiagramGenerator.Verify(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()), Times.Never());
			}
		}

		[TestFixture]
		public class WhenRunningyUMLProcess_OnPathWithSomeProjectFiles_AndHasDependencies
		{
			private yUMLDepend _yUMLDepend;
			private Mock<IProjectFile> _mockProjectFile;
			private Mock<IProjectFileScanner> _mockProjectFileScanner;
			private Mock<IyUMLDiagramGenerator> _mockDiagramGenerator;
			private const string _testBasePath = "test base path";
			private const string _testSavePath = "test save path";
			private List<string> _testFiles = new List<string> { "test project file1", "test project file2" };
			private List<Dependency> _testDependencies = new List<Dependency> { new Dependency("foo", "bar"),
																				new Dependency("foo", "bar"),
																				new Dependency("foo", "bar")};
			private MemoryStream _testFileMemoryStream;
			private MemoryStream _testDiagramMemoryStream;
																			
			[SetUp]
			public void SetUp()
			{
				_testFileMemoryStream = new MemoryStream();
				_testDiagramMemoryStream = new MemoryStream(new byte[] { 1, 2, 3, 4, 5 });

				_mockProjectFile = new Mock<IProjectFile>();
				_mockProjectFileScanner = new Mock<IProjectFileScanner>();
				_mockDiagramGenerator = new Mock<IyUMLDiagramGenerator>();
				
				_mockProjectFileScanner.Setup(m => m.GetProjectFiles(_testBasePath, null))
					.Returns(_testFiles);

				_mockProjectFile.Setup(m => m.GetProjectDependencies())
								.Returns(_testDependencies);

				_mockDiagramGenerator.Setup(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()))
					.Returns(_testDiagramMemoryStream);
				
				_yUMLDepend = new yUMLDepend(f => _testFileMemoryStream,
				                             f => _mockProjectFile.Object,
				                             _mockProjectFileScanner.Object,
				                             _mockDiagramGenerator.Object);
				
				_yUMLDepend.Process(_testBasePath, _testSavePath);
			}
			
			[Test]
			public void ThenTheProjectFileScannerIsUsedToScanBasePath()
			{
				_mockProjectFileScanner.Verify(m => m.GetProjectFiles(_testBasePath, null), Times.Once());
			}
			
			[Test]
			public void ThenEachProjectFileIsInterrogatedForDependencies()
			{
				_mockProjectFile.Verify(m => m.GetProjectDependencies(), Times.Exactly(2));
			}

			[Test]
			public void ThenTheDiagramGeneratorIsCalledOnlyOnceForAllDependencies()
			{
				_mockDiagramGenerator.Verify(m => m.GenerateDiagram(It.IsAny<IEnumerable<Dependency>>()), Times.Once());
			}

			[Test]
			public void ThenTheGeneratedDiagramStreamIsCopiedIntoTheFileStream()
			{
				Assert.That(Enumerable.SequenceEqual(_testDiagramMemoryStream.ToArray(), _testFileMemoryStream.ToArray()));
			}
		}
	}
}

