using System;
using System.Collections.Generic;

namespace yUMLDepend
{
	public interface IProjectFileScanner
	{
		IEnumerable<string> GetProjectFiles(string rootPath, string excludeRegex = null);
	}
}

