using System;

namespace yUMLDepend
{
	public class yUMLDependencyFormatter : IyUMLDependencyFormatter
	{
		public string Format(Dependency dependency) {
			dependency.Guard();
			return string.Format("[{0}]->[{1}]", dependency.Project, dependency.DependentOn);
		}
	}
}

