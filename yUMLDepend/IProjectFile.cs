using System;
using System.Collections.Generic;

namespace yUMLDepend
{
	public interface IProjectFile : IDisposable
	{
		IEnumerable<Dependency> GetProjectDependencies();
	}
}

