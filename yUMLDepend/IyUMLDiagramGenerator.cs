using System;
using System.IO;
using System.Collections.Generic;

namespace yUMLDepend
{
	public interface IyUMLDiagramGenerator
	{
		Stream GenerateDiagram(IEnumerable<Dependency> dependencies);
	}
}

