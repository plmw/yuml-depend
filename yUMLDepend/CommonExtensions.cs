using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace yUMLDepend
{
	public static class CommonExtensions
	{
		public static void Guard(this object obj) {
			if (obj.IsNull()) {
				throw new ArgumentNullException();
			}
		}

		public static bool IsNull(this object obj) {
			return obj == null;
		}

		public static bool IsNotNull(this object obj) {
			return obj != null;
		}

		public static bool Matches(this string str, string pattern) {
			return Regex.IsMatch(str, pattern);
		}

		public static bool DoesNotMatch(this string str, string pattern) {
			return !Regex.IsMatch(str, pattern);
		}

		public static IEnumerable<T> Enumerate<T>(this IEnumerable<T> enumerable) {
			//forces a full enumeration without having to copy contents to another collection
			enumerable.LastOrDefault();
			return enumerable;
		}
	}
}

