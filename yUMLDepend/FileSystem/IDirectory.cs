using System;
using System.Collections.Generic;

namespace yUMLDepend.FileSystem
{
	public interface IDirectory
	{
		IEnumerable<string> GetFiles(string searchPattern);
	}
}

