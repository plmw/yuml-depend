using System;
using System.Collections.Generic;

namespace yUMLDepend.FileSystem
{
	public class Directory : IDirectory
	{
		private readonly string _directoryPath;

		public Directory(string directoryPath) {
			_directoryPath = directoryPath;
		}

		public IEnumerable<string> GetFiles(string searchPattern) {
			return System.IO.Directory.GetFiles(_directoryPath, searchPattern, System.IO.SearchOption.AllDirectories);
		}
	}
}

