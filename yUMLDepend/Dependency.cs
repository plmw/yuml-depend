using System;

namespace yUMLDepend
{
	public class Dependency
	{
		public string Project { get; private set; }
		public string DependentOn { get; private set; }

		public Dependency(string project, string dependentOn)
		{
			project.Guard();
			dependentOn.Guard();

			Project = project;
			DependentOn = dependentOn;
		}
	}
}

