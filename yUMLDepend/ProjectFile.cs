using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Linq;
using System.Xml;

namespace yUMLDepend
{
	public class ProjectFile : IProjectFile
	{
		private const string DefaultNamespace = @"http://schemas.microsoft.com/developer/msbuild/2003";
		private bool _disposed;
		private FileStream _fileStream;

		public ProjectFile(string filePath)
		{
			filePath.Guard();

			_fileStream = File.OpenRead(filePath);

			if (!_fileStream.CanRead) {
				throw new NotSupportedException();
			}
		}

		public IEnumerable<Dependency> GetProjectDependencies()
		{
			XDocument xdocument;

			try {
				xdocument = XDocument.Load(_fileStream);
				
			} catch (XmlException xmlException) {
				throw new NotSupportedException(@"ProjectFile can only be used with XML project files.", xmlException);
			}

			XNamespace defaultNamespace = DefaultNamespace;

			var rootNamespaceElement = xdocument.Descendants(defaultNamespace + "RootNamespace")
										     .FirstOrDefault();

			if (rootNamespaceElement.IsNull()) {
				yield break;
			}

			var rootNamespace = rootNamespaceElement.Value;
			var dependencyElements = xdocument.Descendants(defaultNamespace + "ProjectReference")
												  .Select(e => e.Element(defaultNamespace + "Name"))
												  .Select(n => n.Value);

			foreach (var dependency in dependencyElements) {
				yield return new Dependency(rootNamespace, dependency);
			}
		}

		#region IDisposable implementation

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed) {
				if (disposing) {
					_fileStream.Dispose();
				}
				
				_disposed = true;
			}
		}
		
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}

