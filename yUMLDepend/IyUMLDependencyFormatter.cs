using System;

namespace yUMLDepend
{
	public interface IyUMLDependencyFormatter
	{
		string Format(Dependency dependency);
	}
}

