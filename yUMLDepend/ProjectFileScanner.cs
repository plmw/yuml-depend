using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using yUMLDepend.FileSystem;

namespace yUMLDepend
{
	public class ProjectFileScanner : IProjectFileScanner
	{
		private readonly Func<string, IDirectory> _directoryFactory;

		public ProjectFileScanner(Func<string, IDirectory> directoryFactory) {
			directoryFactory.Guard();
			_directoryFactory = directoryFactory;
		}

		public IEnumerable<string> GetProjectFiles(string rootPath, string excludeRegex = null) {
			var directory = _directoryFactory(rootPath);
			var files = directory.GetFiles("*.csproj");

			var filteredFiles = files.Where(f => excludeRegex.IsNull() ||
			                                	 f.DoesNotMatch(excludeRegex));

			return filteredFiles;
		}
	}
}

