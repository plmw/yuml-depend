using System;
using System.Collections.Generic;
using yUMLDepend.FileSystem;
using System.IO;
using System.Linq;

namespace yUMLDepend
{
	public class yUMLDepend
	{
		private readonly Func<string, Stream> _fileFactory;
		private readonly Func<string, IProjectFile> _projectFileFactory;
		private readonly IProjectFileScanner _projectFileScanner;
		private readonly IyUMLDiagramGenerator _yumlDiagramGenerator;

		public yUMLDepend(Func<string, Stream> fileFactory,
		                  Func<string, IProjectFile> projectFileFactory,
		                  IProjectFileScanner projectFileScanner, 
		                  IyUMLDiagramGenerator yumlDiagramGenerator) {
			_fileFactory = fileFactory;
			_projectFileFactory = projectFileFactory;
			_projectFileScanner = projectFileScanner;
			_yumlDiagramGenerator = yumlDiagramGenerator;
		}

		public void Process(string basePath, string savePath) {
			List<Dependency> allDependencies = new List<Dependency>();

			var projectFiles = _projectFileScanner.GetProjectFiles(basePath);
			
			foreach (var projectFilePath in projectFiles) {
				using (var projectFile = _projectFileFactory(projectFilePath)) {
					var dependencies = projectFile.GetProjectDependencies();
					allDependencies.AddRange(dependencies);
				}
			}

			if (allDependencies.Any()) {
				using (var diagramStream = _yumlDiagramGenerator.GenerateDiagram(allDependencies)) {
					using (var fileStream = _fileFactory(savePath)) {
						diagramStream.CopyTo(fileStream);
					}
				}
			}
		}
	}
}

