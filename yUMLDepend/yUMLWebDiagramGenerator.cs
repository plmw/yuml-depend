using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace yUMLDepend
{
	public class yUMLWebDiagramGenerator : IyUMLDiagramGenerator
	{
		private const string yUMLAddress = "http://yuml.me/diagram/scruffy/class/";
		IyUMLDependencyFormatter _yumlDependencyFormatter;

		public yUMLWebDiagramGenerator(IyUMLDependencyFormatter yumlDependencyFormatter) {
			yumlDependencyFormatter.Guard();
			_yumlDependencyFormatter = yumlDependencyFormatter;
		}

		public Stream GenerateDiagram(IEnumerable<Dependency> dependencies) {
			dependencies.Guard();

			var formattedDependencies = string.Join(",", FormatDepedencies(dependencies));

			var yumlGenerateDiagramRequest = HttpWebRequest.Create(yUMLAddress);
			yumlGenerateDiagramRequest.Method = "POST";

			var dependencyDiagramParameters = new Dictionary<string, string> { { "dsl_text", formattedDependencies } };
			AddParametersToHttpWebRequest(dependencyDiagramParameters, yumlGenerateDiagramRequest);

			var yumlGenerateDiagramResponse = yumlGenerateDiagramRequest.GetResponse();

			using (var yumlResponseReader = new StreamReader(yumlGenerateDiagramResponse.GetResponseStream())) {
				var diagramFileName = yumlResponseReader.ReadToEnd();

				var yumlGetDiagramRequest = HttpWebRequest.Create(yUMLAddress + diagramFileName);
				yumlGetDiagramRequest.Method = "GET";

				var yumlGetDiagramResponse = yumlGetDiagramRequest.GetResponse();

				return yumlGetDiagramResponse.GetResponseStream();
			}
		}

		private void AddParametersToHttpWebRequest(IDictionary<string, string> parameters, WebRequest request) {
			var parameterString = string.Join("&",FormatParameterKeyValuePairs(parameters));

			request.ContentLength = parameterString.Length;
			request.ContentType = "application/x-www-form-urlencoded";

			using (var requestDataStream = request.GetRequestStream())
			{
				requestDataStream.Write(Encoding.UTF8.GetBytes(parameterString), 0, parameterString.Length);
			}
		}

		private IEnumerable<string> FormatParameterKeyValuePairs(IDictionary<string, string> parameters) {
			foreach(var parameter in parameters) {
				yield return string.Format("{0}={1}", parameter.Key, parameter.Value); 
			}
		}

		private IEnumerable<string> FormatDepedencies(IEnumerable<Dependency> dependencies) {
			foreach(var dependency in dependencies) {
				yield return _yumlDependencyFormatter.Format(dependency);
			}
		}
	}
}

