using System;
using System.Collections.Generic;
using System.IO;

namespace yUMLDepend
{
	public class MainClass
	{
		private const string BasePathKey = "BasePath";
		private const string SavePathKey = "SavePath";
		private const int IncorrectArgumentsErrorCode = 1;

		public static void Main(string[] args)
		{	
			if (args.Length < 2) {
				Console.WriteLine("Usage: " + System.AppDomain.CurrentDomain.FriendlyName + " <basePath> <diagramSavePath>");

				System.Environment.Exit(IncorrectArgumentsErrorCode);
			}

			IDictionary<string, string> arguments = ProcessArguments(args);

			//wire-up dependencies for yumlDepend
			var yumlDepend = new yUMLDepend(f => File.Create(f),
			                                f => new ProjectFile(f),
			                                new ProjectFileScanner(f => new FileSystem.Directory(f)),
			                                new yUMLWebDiagramGenerator(new yUMLDependencyFormatter()));

			yumlDepend.Process(arguments[BasePathKey], arguments[SavePathKey]);

			System.Diagnostics.Process.Start(arguments[SavePathKey]);
		}

		private static IDictionary<string, string> ProcessArguments(string[] args) {
			args.Guard();

			var arguments = new Dictionary<string, string>();

			arguments[BasePathKey] = args[0];
			arguments[SavePathKey] = args[1];

			return arguments;
		}
	}
}
