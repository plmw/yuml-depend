using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("yUMLDepend")]
[assembly: AssemblyDescription("Uses yUML.me to generate dependency diagrams of C# Projects.")]
[assembly: AssemblyCopyright("plmw")]

[assembly: AssemblyVersion("1.0.*")]
